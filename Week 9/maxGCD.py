def gcd(a, b):
	if b == 0:
		return a

	return gcd(b, a%b)

def maxGCD(arr):
	ans = 1
	n = len(arr)
	for i in range(n-1):
		for j in range(i+1, n):
			g = gcd(arr[i], arr[j])
			if g > ans:
				ans = g

	return ans

if __name__ == '__main__':
	t = int(input())
	while t:
		arr = list(map(int, input().split()))
		maxGcd = maxGCD(arr)
		print("Maximum GCD is: ",maxGcd)
		t -= 1