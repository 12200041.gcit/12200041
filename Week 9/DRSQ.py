def build(s, e, node):
	# base case
	if s == e:
		stree[node] = arr[s-1] 
		return arr[s-1]

	m = (s+e)//2
	# build left (s, m)
	left = build(s, m, 2*node)
	# build right (m+1, e)
	right = build(m+1, e, 2*node+1)

	# move up from leaf and fill parent nodes/current node
	stree[node] = left + right

	return stree[node]

# find range sum
def sum(s, e, l, r, node):
	# no overlap
	if e < l or s > r:
		return 0

	# complete overlap
	if s >= l and e <= r:
		return stree[node] 

	# partial overlap
	mid = (s+e)//2
	q1 = sum(s, mid, l, r, 2*node)
	q2 = sum(mid+1, e, l, r, 2*node+1)

	return q1+q2

def update(s, e, pos, val, node):
    if pos > e or pos < s:
    	return

    if s == e and s == pos:
    	stree[node] = val
    	return
    
    mid = (s+e)//2
    if pos <= mid:
    	update(s, mid, pos, val, 2*node) # left
    else:
    	update(mid+1, e, pos, val, 2*node+1) # right

    # update ancestors
    stree[node] = stree[2*node]+stree[2*node+1]


def buildTree():
	return build(1, n, 1) # root index = 1

def querySum(l, r):
	return sum(1, n, l, r, 1)

def queryUpdate(index, val):
	return update(1, n, index, val, 1)

if __name__ == '__main__':
	n, q = map(int, input().split())
	stree = [0]*(4*n)
	arr = list(map(int, input().split()))
	buildTree()
	while q:
		t, a, b = map(int, input().split())
		if t == 1:
			queryUpdate(a, b)
		else:
			print(querySum(a, b))
		q -= 1
