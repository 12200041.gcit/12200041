# build pascals triangle
def pascalsT(n):
	table = [[0]*n for i in range(n)]
	# for row n
	for i in range(n):
		for r in range(i+1): # for column r
			if i == r or r == 0:
				# nCn or =nC0 = 1
				table[i][r] = 1
			else:
				table[i][r] = table[i-1][r-1] + table[i-1][r]

	return table

if __name__ == '__main__':
	N = 301 # max n value
	table = pascalsT(N)
	n, w, t, r = map(int, input().split())
	print((table[n][w]*table[n-w][t]*table[n-w-t][r])%1000000007)