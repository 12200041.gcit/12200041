# return a^n value, binomial exponentiation
def power(a, n):
    result = 1
    while n:
        if n & 1:
            result = (result*a)%mod
        a = (a*a) % mod
        n >>= 1
    return result

# returns substring, L to R hash value in O(1)
def substringHash(L, R):
    result = prefix[R]
    if L>0:
        result -= prefix[L-1]
    result = (result * MMIP[L])%mod
    
    return result

if __name__ == '__main__':
    p = 31
    mod = 10**9+7
    pPow = 1
    s = "ababab"
    n = len(s)
    MMIP = [0]*n
    prefix = [0]*n

    for i in range(n):
        # multiplicative modulo inverse of p^i
        MMIP[i] = power(pPow, mod-2) # a/b%p=(a%p)*(b^p-2%p)
        prefix[i] = (prefix[i-1]+(ord(s[i])-ord('a')+1)*pPow) % mod
        pPow = pPow*p

    tc = int(input())
    while tc:
        L, R = map(int, input().split())
        print(substringHash(L, R))
        tc -= 1
    