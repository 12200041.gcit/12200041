# returns hash value of s
# implementation of ploynomial hash function
def computeHash(s):
	p = 31
	m = 10**9+7
	hashValue = 0 # hash Value of s
	pPow = 1 # p^0 =1

	# precompute hash value of whole string O(n)
	for ch in s:
		hashValue = (hashValue + (ord(ch)-ord('a')+1)*pPow)%m 
		# ch = c, 99-97+1 = 3
		pPow = (pPow * p)%m # p power increases by 1

	return hashValue

if __name__ == '__main__':
	s1 = "coding"
	s2 = "fix"
	print(computeHash(s1))
	print((computeHash(s2)))