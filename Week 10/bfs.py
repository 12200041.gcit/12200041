import collections

class Graph:
	# constructor
	def __init__(self):
		self.graph = collections.defaultdict(list)

	def add_edge(self, src, dest):
		self.graph[src].append(dest)
		self.graph[dest].append(src)

	def bfs(self, src, dest):
		queue = collections.deque([src]) # deque: list-like container with fast appends and pops on either end.

		visited = set() # visited nodes
		visited.add(src)
		path = list()
		while queue:
			currentNode = queue.popleft() # remove from left
			path.append(currentNode)
			# traverse all adj nodes of currentNode
			for node in self.graph[currentNode]:
				if node not in visited:
					queue.append(node)
					visited.add(node)
		
		return path

if __name__ == '__main__':
	g = Graph() # creating instance
	g.add_edge(1, 2)
	g.add_edge(1, 5)
	g.add_edge(2, 3)
	g.add_edge(2, 4)
	g.add_edge(5, 6)
	# print(g.graph)
	print(g.bfs(1, 6))